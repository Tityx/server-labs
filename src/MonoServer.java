import java.io.*;
import java.net.*;

public class MonoServer implements Runnable{

    private Thread thread;
    private Socket client;
    private ObjectOutputStream out;
    private ObjectInputStream in;

    MonoServer(Socket client) throws IOException{
        this.client = client;
        thread = new Thread(this, "MonoServer");
        print_message("Клиент " + client.getRemoteSocketAddress().toString() + " подключился");
        print_message("Сейчас в сети " + (Server.serverList.size()+1) + " клиентов");
        out = new ObjectOutputStream(client.getOutputStream());
        in = new ObjectInputStream(client.getInputStream());
        thread.start();
    }

    @Override
    public void run(){
        try{

            while (true){
                Object tmp = in.readObject();
                if(tmp.equals("quite")){
                    close_client();
                    break;
                }else if(tmp.equals("get users")){
                    sendUsers();
                }else if(tmp.equals("send bees")){
                    String toClient =  (String) in.readObject();
                    for(MonoServer sv : Server.serverList) {
                        if (toClient.equals(sv.client.getRemoteSocketAddress().toString())) {
                            sv.sendData("sending bees");
                            sv.sendData(in.readObject());
                            print_message("Клиент " + sv.client.getRemoteSocketAddress() + " получил объекты");
                        }
                    }
                }
            }
        }catch (IOException e) {
            e.printStackTrace();
        }catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    void close_client(){
        print_message("Клиент " + client.getRemoteSocketAddress().toString() + " отключился");
        Server.serverList.remove(this);
        print_message("Осталось " + Server.serverList.size() + " клиентов");
        try {
            in.close();
            out.close();
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    void sendUsers(){
        sendData("sending users");
        String users[] = new String[Server.serverList.size()];
        int count = 0;
        for(MonoServer sv : Server.serverList){
            if(!sv.client.getRemoteSocketAddress().equals(client.getRemoteSocketAddress()))
                users[count++] = sv.client.getRemoteSocketAddress().toString();
        }
        try {
            out.writeObject(users);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    void sendData(Object obj){
        try {
            out.writeObject(obj);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    void print_message(String message){
        Server.consoleText.append(message + "\n");
    }
}
