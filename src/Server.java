import javax.swing.*;
import java.net.*;
import java.io.*;
import java.util.LinkedList;

public class Server extends JFrame implements Runnable {

    public static JTextArea consoleText = new JTextArea();
    private Thread thread;
    private ServerSocket server;
    private Socket client;
    public static LinkedList<MonoServer> serverList = new LinkedList<>();

    public Server(){
        super("Server");
        thread = new Thread(this, "Server");
        this.setSize(600, 400);
        this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
        consoleText.setLineWrap(true);
        this.add(new JScrollPane(consoleText));
        this.setVisible(true);
        thread.start();
    }

    @Override
    public void run() {
        try {
            server = new ServerSocket(1211);

            while(true){
                client = server.accept();
                serverList.add(new MonoServer(client));
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}